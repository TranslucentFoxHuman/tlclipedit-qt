# TlClipEdit-Qt ビルド方法
 

## 共通の準備
git clone [https://gitlab.com/TranslucentFoxHuman/memfat.git](https://gitlab.com/TranslucentFoxHuman/memfat.git)  
を実行し、リポジトリをクローンします。  
そうしたら、クローンしたリポジトリのディレクトリに移動してください。  
 
すでにあるTlClipEdit-Qtをアップグレードするには、TlClipEdit-Qtのディレクトリに移動し、Buildディレクトリを削除した後、  
git pull  
を実行し、最新の状態に更新します。  
 
## GNU/Linux  
### ビルドの準備
TlClipEdit-Qtをビルドするには、以下のパッケージが必要です (DebianベースOSの場合)  
qt5-default g++ make
  
上記のパッケージをインストールしたら、  
mkdir Build && cd Build  
qmake ..  
make  
の順に実行し、ソフトウェアをビルドします。
  
ビルド完了後、  
sudo make install  
でインストールが可能です。自動でバイナリファイルとアイコン、デスクトップエントリなどが配置されます。
  
## FreeBSD
qt5-qmake qt5-svg make g++  
が必要です。  
  
上記のパッケージをインストールしたら、GNU/Linux同様 
mkdir Build && cd Build  
qmake ..  
make  
の順に実行しソフトウェアをビルドします。  
  
ビルド完了後、  
sudo make install  
でインストールが可能です。

## Microsoft Windows
MinGWとQtをセットアップします。    
Qtのセットアップが終わったら、Qtのデベロッパーコマンドプロンプトを開き、クローンしたリポジトリのディレクトリに移動し、  
MD Build  
CD Build  
qmake ..  
mingw32-make  
でビルド可能です。   
 
ライブラリ群を用意するには、ビルドして出力されたtlclipedit-qt.exeを何もないフォルダに移動し、そのフォルダーでQt デベロッパーコマンドプロンプトを開き  
windeployqt tlclipedit-qt.exe  
これで、exeのあるフォルダに、dllなどの必要なファイルが自動で配置されます。

