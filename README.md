# TlClipEdit-Qt
簡易的かつシンプルなクリップボードのテキストを編集するツール  
  
TlClipEdit-Qtは、簡易的なクリップボードのテキストを編集するツールです。  

## 以下の機能があります  
・クリップボードのテキストを表示  
・クリップボードにテキストをコピー  
・一時的にクリップボードの内容を履歴へとどめておく  

## ダウンロード
Windows(64bit) : [tlclipedit-qt_amd64.7z](https://gitlab.com/TranslucentFoxHuman/tlclipedit-qt/-/raw/release/tlclipedit-qt_amd64.7z)  
GNU/Linux AppImage(64bit) : [TlClipEdit-x86_64.AppImage](https://gitlab.com/TranslucentFoxHuman/tlclipedit-qt/-/raw/release/TlClipEdit-x86_64.AppImage)  
Android : [net.tlfoxhuman.tlclipedit_qt.apk](https://gitlab.com/TranslucentFoxHuman/tlclipedit-qt/-/raw/release/net.tlfoxhuman.tlclipedit_qt.apk)  
  
## サポートされるプラットフォーム  
・GNU/Linux (Debian 10, Ubuntu 20.04でテスト済み)  
・Microsoft Windows (Wineにてビルド、Windows 10でテスト済み)  
・Android (5.1, 11にてテスト済み)  
・FreeBSD (FreeBSD 13にてテスト済み)  
 
## ライセンス  
このプログラムはフリーソフトウェアです。あなたはこれを、フリーソフトウェア財団によって発行されたGNU 一般公衆利用許諾書（バージョン3か、それ以降のバージョンのうちどれか）が定める条件の下で再頒布または改変することができます。  
このプログラムは有用であることを願って頒布されますが、*全くの無保証*です。*商業可能性の保証や特定目的への適合性は、言外に示されたものも含め、全く存在しません。*詳しくはGNU 一般公衆利用許諾書をご覧ください。あなたはこのプログラムと共に、GNU 一般公衆利用許諾書のコピーを一部受け取っているはずです。もし受け取っていなければ、< [https://www.gnu.org/licenses/](https://www.gnu.org/licenses/) > をご覧ください。  